FROM alpine:latest

RUN apk update && apk upgrade && apk add bash bash-doc bash-completion 

RUN apk add --no-cache --virtual git && \
    apk add --update nodejs nodejs-npm && \
    npm install -g @angular/cli

RUN mkdir angular-app && \
    cd angular-app && \
    git clone https://gokulresearcher@bitbucket.org/gokulresearcher/poker-angular.git && \
    cd poker-angular && \
    npm install 
    
